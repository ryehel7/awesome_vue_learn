new Vue({
    el:'#vue-app', //element that called on html div
    data: {
        name: '',
        job: '',
        age: '',
        website: 'http://www.facebook.com',
        websiteTag:'<a href="http://www.facebook.com">Facebook</a>',
        x: 0,
        y: 0,

        //Dynamic css
        available: true,
        nearby: false
    },

    methods: {
        greet: function(time){
            return 'Good ' + time + ' ' + this.name;
        },

        add: function(inc){
            this.age += inc;
        },

        subtract: function(dec){
            this.age -= dec;
        },

        updateXY: function(event){
        //console.log(event);
            this.x = event.offsetX;
            this.y = event.offsetY;
        },

        link_click: function(){
            alert('Aw. You clicked me !');
        }
    },

    computed: {
        /*Part Dynamic CSS*/
        compClasses: function(){
            return{
                available: this.available,
                nearby: this.nearby
            }
        }
    }



});
